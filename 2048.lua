
--初始化函数
function init()
    gridData = {}
    math.randomseed(tostring(os.time()):reverse():sub(1, 7)) --设置时间种子
    for i = 1,4 do
        gridData[i] = {}
        for j = 1,4 do
            gridData[i][j] = " "
        end
    end
end

--打印输出，调试用的
function printData ()
    for i = 1,4 do
        for j = 1,4 do
           io.write(tostring(gridData[i][j]))
        end
        io.write("\n")
    end
end

--输出网格数据
function drawGrid()
    io.write("  -------------------------\n")
    for i = 1,4 do
        io.write("  ")
        for j = 1,4 do
            io.write("|  "..tostring(gridData[i][j]).."  ")
        end
        io.write("|\n")
        io.write("  -------------------------\n")
    end
end

--产生新数据
function newData()
    repeat
        i = math.random(1,4)
        j = math.random(1,4)
    until gridData[i][j] == " "
    gridData[i][j] = 2
end

--获取并执行用户的输入动作
function moveAction()
    local input = io.read()
    if input == "w" then
    elseif input == "a" then
        for i = 1,4 do
            for j = 2,4 do
                if gridData[i][j] ~= " " and gridData[i][j] == gridData[i][j-1] then
                    gridData[i][j-1] = tonumber(gridData[i][j-1]) + tonumber(gridData[i][j])
                    gridData[i][j] = " "
                end
            end
        end
    elseif input == "s" then
    elseif input == "d" then
    else
    end
end

init()

--add at surface.
--add at S7
